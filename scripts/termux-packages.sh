#!/data/data/com.termux/files/usr/bin/bash

curl -LO https://its-pointless.github.io/setup-pointless-repo.sh
chmod +x ./setup-pointless-repo.sh
bash ./setup-pointless-repo.sh

pkg upgrade

pkg install root-repo
pkg install unstable-repo

pkg install \
    apache2 \
    apksigner \
    arp-scan \
    autoconf \
    bash-completion \
    bison \
    busybox \
    cgdb \
    cmake \
    dnsutils \
    e2fsprogs \
    emacs \
    exiftool \
    ffmpeg \
    git \
    gcc-10 \
    gdb \
    gdbm \
    htop \
    hwinfo \
    ltrace \
    m4 \
    man \
    mpv \
    mariadb \
    mlocate \
    nano \
    nbtscan \
    ncurses \
    ncurses-utils \
    nmap \
    openssh \
    p7zip \
    pciutils \
    php \
    pigz \
    pkg-config \
    qemu-system-aarch64-headless \
    qemu-system-arm-headless \
    qemu-system-x86_64-headless \
    qemu-user-aarch64 \
    qemu-user-arm \
    qemu-user-x86_64 \
    qemu-utils \
    rsync \
    runit \
    samba \
    samefile \
    socat \
    sqlite \
    strace \
    stunnel \
    syncthing \
    tcpdump \
    termux-api \
    termux-services \
    texinfo \
    tsu \
    vim \
    wavemon \
    wget \
    youtubedr

termux-setupgcc-10
