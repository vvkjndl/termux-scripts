#!/data/data/com.termux/files/usr/bin/bash

backdir=/data/data/com.termux/files/home/backup/
backfile=backup-<preferred-filename-suffix>
backinclude=/data/data/com.termux/files/home/scripts/backup-files-include
backexclude=/data/data/com.termux/files/home/scripts/backup-files-exclude

# tar backup with gzip and checksum generation
tar --create --dereference --verbose --exclude-from=$backexclude --file - --files-from=$backinclude | pigz --best > $backdir/$backfile.tar.gz
echo "$(sha512sum $backdir/$backfile.tar.gz | cut -d ' ' -f1)  $backfile.tar.gz" > $backdir/$backfile.tar.gz.sha512sum
