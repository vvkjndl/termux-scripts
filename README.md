# Summary

These are easy to use termux scripts to start smbd or sshd daemons right from the home screen via widget. The init scripts included does all handling of starting and stopping the daemons.


# Warning

Only start the daemons when you are on a trusted network. Configuration files are not tested in terms of security with little to no hardening being done.


# Requirements

* root is needed for smbd.
* [termux](https://f-droid.org/en/packages/com.termux/).
* [termux widget](https://f-droid.org/en/packages/com.termux.widget/).
* run the termux-setup-storage command after the app installation to make sure termux has access to internal storage.

> $ termux-setup-storage


# Screenshots

![](assets/screenshots/homescreen.png)
![](assets/screenshots/termux.png)


# Features

* start/stop the daemons right from your home screen.
* interactive choice menu to start/stop the services.
* binds to specified single interface only.
* includes backup script.


# Usage

Just clone this repository in your termux home directory (~) and then bring all directory contents in to parent directory.

> $ git clone https://gitlab.com/vvkjndl/termux-scripts.git

> $ mv ./termux-scripts/scripts/ ./

> $ mv ./termux-scripts/conf.d/ ./

> $ mv ./.termux-scripts/.shortcuts/ ./

> $ rm -rf ./termux-scripts/

> $ chmod +x ./scripts/termux-packages.sh

Now run the termux-packages.sh to pull the needed packages. The script in fact pulls a lot of packages and sets the default compiler from clang to gcc.

**(Optional)** Edit the termux-packages.sh to exclude any package you don't want to install or to comment the gcc setup lines.

> $ nano ./scripts/termux-packages.sh

> $ ./scripts/termux-packages.sh

Now connect to your trusted wireless network and verify the name of the interface.

> $ ip address show up

Update the config files with the name of wireless interface.

> $ nano ./conf.d/sshd.ipv4address

> $ nano ./conf.d/smb.conf

Update the daemon's main configuration files as per your specifications like port or shares etc.

> $ nano ./conf.d/smb.conf

> $ nano ./conf.d/sshd_config

**Note:** sshd init script will automatically update the ListenAddress in sshd_config by picking up ip from interface as specified in ./conf.d/sshd.ipv4address.


# Adding smbd users

> $ sudo /data/data/com.termux/files/usr/bin/smbpasswd -a -c /data/data/com.termux/files/home/conf.d/smb.conf username


# Listing smbd users

> $ sudo /data/data/com.termux/files/usr/bin/pdbedit --list --verbose --configfile=/data/data/com.termux/files/home/conf.d/smb.conf


# Deleting smbd users

> $ sudo /data/data/com.termux/files/usr/bin/smbpasswd -x -c /data/data/com.termux/files/home/conf.d/smb.conf username


# Starting services

Add the termux widget to your homescreen. Scripts will be visible there. You can now start/stop the services with just a tap.


# Notes

* init scripts will not start daemons if specified interface has no ip.
* use domain\username and password to mount smbd share on client side.


# To-do

* configuration files hardening.
* log rotation and cleanup.
* prevent killing of daemons by android.
* make root storage rw.
* match process name during killing.